package com.everis.data.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.everis.data.models.Producto;
import com.everis.data.models.Usuario;
import com.everis.data.models.Venta;
import com.everis.data.services.ProductoService;
import com.everis.data.services.UsuarioService;
import com.everis.data.services.VentaService;

@Controller
@RequestMapping("/venta")
public class VentaController {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ProductoService productoService;
	
	@Autowired
	private VentaService ventaService;
	
	@RequestMapping("")
	public String index(@ModelAttribute("usuario") Usuario usuario ,Model model) {
	
		model.addAttribute("lista_productos", productoService.findAll());
		return "venta.jsp";
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public String venta(@PathVariable("id") Long id, Model model) {
		System.out.println("El id de usuario es: "+id);
		Usuario usuario = usuarioService.buscarUsuario(id);
		model.addAttribute("usuario", usuario);
		return "redirect:/venta";
	}
	

}

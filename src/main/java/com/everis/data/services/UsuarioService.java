package com.everis.data.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.data.models.Usuario;
import com.everis.data.repositories.UsuarioRepository;

@Service
public class UsuarioService {
	
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	public void crearUsuario(@Valid Usuario usuario) {			
		usuarioRepository.save(usuario);
	}
	
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}
	
	public void eliminarUsuario(Long id) {
		usuarioRepository.deleteById(id);		
	}
	
	public Usuario buscarUsuario(Long id) {
		Optional<Usuario> oUsuario = usuarioRepository.findById(id);
		if(oUsuario.isPresent()) {
			return oUsuario.get();
		}
		return null;
	}
	
	public void modificarPersona(@Valid Usuario usuario) {	
		usuarioRepository.save(usuario);
	}

}
